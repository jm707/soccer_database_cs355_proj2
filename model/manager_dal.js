var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM manager '
    + 'ORDER BY first_name, last_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (manager_id, callback) {
  var query = 'SELECT m.*, c.country_name, t.team_name FROM country c '
    + 'LEFT JOIN manager m ON c.country_id = m.country_id '
    + 'LEFT JOIN team t ON t.manager_id = m.manager_id '
    + 'WHERE m.manager_id = ?';

  var queryData = [manager_id];

  connection.query(query, queryData, function (err, result) {
      callback(err, result);
  });
};

exports.update = function (params, callback) {
  var query = 'UPDATE manager SET first_name = ?, last_name = ?, age = ?, country_id = ? '
    + 'WHERE manager_id = ?';
  var queryData = [params.first_name, params.last_name, params.age, params.country_id, params.manager_id];

  connection.query(query, queryData, function (err, result) {
        callback(err, params.first_name, params.last_name);
    });
};

exports.insert = function (params, callback) {

  var query = 'INSERT INTO manager (first_name, last_name, age, country_id) '
    + 'VALUES(?,?,?,?)';
  var queryData = [params.first_name, params.last_name, params.age, params.country_id];

  connection.query(query, queryData, function (err, result) {
      callback(err, params.first_name, params.last_name);
  });
};

exports.delete = function (manager_id, callback) {

    var query = 'DELETE FROM manager WHERE manager_id = ?';
    var queryData = [manager_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};