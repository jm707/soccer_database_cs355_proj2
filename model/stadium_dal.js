var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM stadium '
    + 'ORDER BY stadium_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (stadium_id, callback) {
    var query = 'SELECT s.*, t.team_name FROM stadium s ' +
        'LEFT JOIN team t ON t.stadium_id = s.stadium_id ' +
        'WHERE s.stadium_id = ?';

    var queryData = [stadium_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback) {

    var query = 'INSERT INTO stadium (stadium_name, address) '
        + 'VALUES(?,?)';
    var queryData = [params.stadium_name, params.address];

    connection.query(query, queryData, function (err, result) {
        callback(err, params.stadium_name);
    });
};

exports.update = function (params, callback) {
    var query = 'UPDATE stadium SET stadium_name = ?, address = ? '
        + 'WHERE stadium_id = ?';
    var queryData = [params.stadium_name, params.address, params.stadium_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, params.stadium_name);
    });
};

exports.delete = function (stadium_id, callback) {

    var query = 'DELETE FROM stadium WHERE stadium_id = ?';
    var queryData = [stadium_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};