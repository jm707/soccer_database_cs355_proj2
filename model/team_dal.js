var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT t.*, m.first_name AS mf_name, m.last_name AS ml_name FROM team t '
    + 'LEFT JOIN manager m ON m.manager_id = t.manager_id '
    + "ORDER BY t.team_name";

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getHavePlayers = function (callback) {
    var query ='SELECT a.team_id, a.Team FROM ' +
        '(SELECT t.team_id, t.team_name AS Team, COUNT(p.player_id) AS NumPlayersOnTeam FROM team t ' +
        'LEFT JOIN player p ON p.team_id = t.team_id ' +
        'GROUP BY t.team_name) a ' +
        'WHERE a.NumPlayersOnTeam != 0';

    connection.query(query, function(err, result) {
        callback(err, result);
    });

};

exports.insert = function (params, callback) {

    var query = 'INSERT INTO team (team_name, home_color, away_color, third_color, stadium_id, manager_id) '
    + 'VALUES (?,?,?,?,?,?)';

    var queryData = [params.team_name, params.home_color, params.away_color, params.third_color, params.stadium_id,
    params.manager_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, params.team_name);
    });

};

exports.update = function (params, callback) {
  var query = 'UPDATE team SET team_name = ?, home_color = ?, away_color = ?, third_color = ?, stadium_id = ?, manager_id = ? '
    + 'WHERE team_id = ?';
  var queryData = [params.team_name, params.home_color, params.away_color, params.third_color, params.stadium_id,
  params.manager_id, params.team_id];

  connection.query(query, queryData, function (err, result) {
      callback(err, params.team_name);
  });

};

exports.getById = function (team_id, callback) {

    var query = 'CALL team_getinfo(?)';
    var queryData = [team_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function (team_id, callback) {

    var query = 'DELETE FROM team WHERE team_id = ?';
    var queryData = [team_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.getStats = function (callback) {
  var query = 'SELECT t.team_id, t.team_name AS Team, ' +
      'COUNT(p.player_id) AS NumPlayersOnTeam, ' +
      ' AVG(p.shooting) AS AVGShooting, ' +
      'AVG(p.passing) AS AVGPassing, ' +
      'AVG(p.pace) AS AVGPace, ' +
      'AVG(p.defending) AS AVGDefending, ' +
      'AVG(p.physical) AS AVGPhysical, ' +
      'AVG(p.dribbling) AS AVGDribbling ' +
      'FROM team t ' +
      'LEFT JOIN player p ON p.team_id = t.team_id ' +
      'GROUP BY t.team_name ' +
      'ORDER BY NumPlayersOnTeam DESC';
  connection.query(query, function(err, result) {
      callback(err, result);
  });
};

exports.getPlayersForTeam = function (team_id, callback) {
  var query = 'CALL team_get_players(?)';
  var queryData = [team_id];
  connection.query(query, queryData, function(err, result) {
      callback(err, result);
  });
};

exports.getGamesForTeam = function (team_id, callback) {
    var query = 'SELECT g.*, t.team_name AS HomeTeam, w.team_name AS AwayTeam FROM game g ' +
        'LEFT JOIN team t ON g.teamH_id = t.team_id ' +
        'LEFT JOIN team w ON g.teamA_id = w.team_id ' +
        'WHERE g.teamH_id = ? OR g.teamA_id = ?';
    var queryData = [team_id, team_id];

    connection.query(query, queryData, function(err, games) {

        var query = 'SELECT t.team_name FROM team t WHERE t.team_id = ?';
        var queryData = [team_id];
        connection.query(query, queryData, function(err, teamName) {
            callback(err, games, teamName);
        });
    });
};

exports.getLogo = function (team_id, callback) {
    var query = 'SELECT link FROM logo WHERE team_id = ?';
    connection.query(query, team_id, function(err, linkLogo) {
        callback(err, linkLogo);
    });
};