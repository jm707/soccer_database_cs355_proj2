var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM referee '
    + 'ORDER BY first_name, last_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (ref_id, callback) {
    var query = 'SELECT r.*, c.country_name FROM country c ' +
        'LEFT JOIN referee r ON c.country_id = r.country_id ' +
        'WHERE r.ref_id = ?';

    var queryData = [ref_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback) {

    var query = 'INSERT INTO referee (first_name, last_name, age, country_id) '
        + 'VALUES(?,?,?,?)';
    var queryData = [params.first_name, params.last_name, params.age, params.country_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, params.first_name, params.last_name);
    });
};

exports.update = function (params, callback) {
    var query = 'UPDATE referee SET first_name = ?, last_name = ?, age = ?, country_id = ? '
        + 'WHERE ref_id = ?';
    var queryData = [params.first_name, params.last_name, params.age, params.country_id, params.ref_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, params.first_name, params.last_name);
    });
};

exports.delete = function (ref_id, callback) {

    var query = 'DELETE FROM referee WHERE ref_id = ?';
    var queryData = [ref_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.getGames = function (callback) {
  var query = 'SELECT * FROM view_ref_num_games';
  connection.query(query, function(err, result) {
      callback(err, result);
  });
};

exports.getGamesForRef = function(ref_id, callback) {
  var query = 'SELECT g.match_id, t.team_name AS HomeTeam, ' +
      'w.team_name AS AwayTeam, g.week, g.winner, g.scoreH, g.scoreA, r.first_name, r.last_name ' +
      'FROM game g ' +
      'LEFT JOIN match_ m ON m.match_id = g.match_id ' +
      'LEFT JOIN referee r ON m.ref_id = r.ref_id ' +
      'LEFT JOIN team t ON t.team_id = g.teamH_id ' +
      'LEFT JOIN team w ON w.team_id = g.teamA_id ' +
      'WHERE r.ref_id = ?';

  var queryData = [ref_id];
  connection.query(query, queryData, function(err, result) {
      callback(err, result);
  });
};