var express = require('express');
var router = express.Router();

var game_dal = require('../model/game_dal');
var team_dal = require('../model/team_dal');
var referee_dal = require('../model/referee_dal');

// View All games
router.get('/all', function(req, res) {
    game_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('game/gameViewAll', { 'result':result });
        }
    });

});

router.get('/add', function (req, res) {
    team_dal.getAll(function(err,teams) {
        if (err) {
            res.send(err);
        }
        else {
            referee_dal.getAll(function (err,refs) {
               if (err) {
                   res.send(err);
               }
               else {
                   res.render('game/gameAdd', {'teams': teams, 'refs': refs});
               }
            });
        }
    });
});

router.post('/insert', function(req, res) { //Changed req.query to req.body !
    // simple validation
    if (req.body.home_team_id == null || req.body.away_team_id == null || req.body.ref_id == null) {
        res.send('Home and Away Teams, and ref must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        game_dal.insert(req.body, function (err, homeTeam, awayTeam) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                game_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('game/gameViewAll', {
                            'result': result,
                            'homeTeam': homeTeam,
                            'awayTeam': awayTeam,
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.match_id == null) {
        res.send('A match id is required');
    }
    else {
        game_dal.getById(req.query.match_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('game/gameUpdate', {
                    'game':result[0]
                });
            }
        });
    }
});

router.get('/update', function (req, res) {
   if (req.query.match_id == null) {
       res.send('A match id is required');
   }
   else {
       game_dal.update(req.query, function(err, homeTeam, awayTeam){
           if (err) {
               res.send(err);
           }
           else {
               game_dal.getAll(function (err, result) {
                   if (err) {
                       res.send(err);
                   }
                   else {
                       res.render('game/gameViewAll', {
                           'result': result,
                           'homeTeam': homeTeam,
                           'awayTeam': awayTeam,
                           'edit_successful': true
                       });
                   }
               });
           }
       });
   }
});


router.get('/delete', function (req, res) {
    if(req.query.match_id == null) {
        res.send('match id is null');
    }
    else {
        game_dal.delete(req.query.match_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                // change to say success?
                res.redirect(302, '/game/all');
            }
        });
    }
});

router.get('/riggedGames', function (req,res) {
    game_dal.biased(function (err, result) {
        if(err) {
            res.send(err);
        }
        else {
            res.render('game/viewRiggedGames', { 'result':result });
        }
    });
});

router.get('/nogames', function (req, res) {
   game_dal.getNoGames(function (err, result) {
       if (err) {
           res.send(err);
       }
       else {
           res.render('game/viewNoGames', { 'teams':result });
       }
   }) ;
});

module.exports = router;
