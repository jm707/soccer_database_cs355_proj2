var express = require('express');
var router = express.Router();

var team_dal = require('../model/team_dal');
var manager_dal = require('../model/manager_dal');
var stadium_dal = require('../model/stadium_dal');

// View All teams
router.get('/all', function(req, res) {
    team_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('team/teamViewAll', { 'result':result });
        }
    });

});

// View the team for the given team_id
router.get('/', function (req, res) {
    if (req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.getById(req.query.team_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                team_dal.getLogo(req.query.team_id, function (err, logoLink) {
                    if(err) {
                        res.send(err)
                    }
                    else {
                        res.render('team/teamViewById', {'team': result[0][0], 'stadium': result[1][0], 'manager': result[2][0], 'image':logoLink[0]});
                    }
                });
            }
        });
    }
});

router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    manager_dal.getAll(function (err, managers) {
        if (err) {
            res.send(err);
        }
        else {
            stadium_dal.getAll(function (err, stadiums) {
                if (err) {
                    res.send(err);
                }

                else {
                    res.render('team/teamAdd', {
                        'managers': managers,
                        'stadiums': stadiums
                    });
                }
            });
        }
    });
});

router.post('/insert', function(req, res) { //Changed req.query to req.body !
    // simple validation
    if (req.body.team_name == null) {
        res.send('Team name must be provided.');
    }
    else {
        team_dal.insert(req.body, function (err, teamName) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                team_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('team/teamViewAll', {'result': result, 'team': teamName, 'was_successful': true});
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.team_id == null) {
        res.send('A team id is required');
    }
    else {
        team_dal.getById(req.query.team_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                stadium_dal.getAll(function (err, stadiums) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        manager_dal.getAll(function (err, managers) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('team/teamUpdate', {
                                    'team':result[0][0],
                                    'stadiums': stadiums,
                                    'managers': managers
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

router.get('/update', function (req,res) {
    team_dal.update(req.query, function(err, teamName){

        if (err) {
            res.send(err);
        }
        else {

            team_dal.getAll(function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('team/teamViewAll', {'result': result, 'team': teamName, 'edit_successful': true});
                }
            });
        }
    });
});

router.get('/delete', function(req, res){
    if(req.query.team_id == null) {
        res.send('team_id is null');
    }
    else {
        team_dal.delete(req.query.team_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {

                // change to say success?
                res.redirect(302, '/team/all');
            }
        });
    }
});

router.get('/avgstats', function (req, res) {
    team_dal.getStats(function (err,result) {
        if (err){
            res.send(err);
        }
        else {
            res.render('team/allTeamStats', {'teams': result});
        }
    });
});

router.get('/viewPlayersForTeam', function (req, res) {
   if (req.query.team_id == null) {
       res.send('team id is null');
   }
   else {
       team_dal.getPlayersForTeam(req.query.team_id, function (err, players) {
           if (err) {
               res.send(err);
           }
           else if (players[0][0].first_name == null) {
               res.send('Sorry, this team does not have any players!');
           }
           else {
               res.render('team/viewPlayersTeam', {'players': players[0]});
           }
       });
   }
});

router.get('/viewGamesForTeam', function (req, res) {
    if (req.query.team_id == null) {
        res.send('team id is null');
    }
    else {
        team_dal.getGamesForTeam(req.query.team_id, function (err, games, team_name) {
            if (err) {
                res.send(err);
            }
            else if (games[0] == null) {
                res.send('Sorry, this team does not have any games!');
            }
            else {
                res.render('team/viewGamesTeam', {'games': games, 'team': team_name[0].team_name});
            }
        });
    }
});

router.get('/uploadForTeam', function (req, res) {
   if (req.query.team_id == null) {
       res.send('team id is null')
   }
   else {
       res.render('team/uploadCrest', {'team': req.query.team_id});
   }
});

module.exports = router;
