var express = require('express');
var router = express.Router();

router.get('/', function (req, res) { // View the about page
    res.render('about/about');
});

module.exports = router;