var express = require('express');
var router = express.Router();

var referee_dal = require('../model/referee_dal');
var country_dal = require('../model/country_dal');

// View All referees
router.get('/all', function(req, res) {
    referee_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('referee/refereeViewAll', { 'result':result });
        }
    });

});

router.get('/', function (req, res) {
    if (req.query.ref_id == null) {
        res.send('ref_id is null');
    }
    else {
        referee_dal.getById(req.query.ref_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('referee/refereeViewById', {'referee': result[0]});
            }
        });
    }
});


router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    country_dal.getAll(function (err, countries) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('referee/refereeAdd', {
                'countries': countries
            });
        }
    });
});

router.post('/insert', function(req, res) { //Changed req.query to req.body !
    // simple validation
    if (req.body.first_name == null || req.body.last_name == null || req.body.country_id == null || req.body.age == null) {
        res.send('First name, last name, age, and country must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        referee_dal.insert(req.body, function (err, refFName, refLName) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                referee_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('referee/refereeViewAll', {
                            'result': result,
                            'refF': refFName,
                            'refL': refLName,
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.ref_id == null) {
        res.send('A ref id is required');
    }
    else {
        referee_dal.getById(req.query.ref_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                country_dal.getAll(function (err, countries) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('referee/refereeUpdate', {
                            'referee':result[0],
                            'countries': countries
                        });
                    }
                });
            }
        });
    }
});

router.get('/update', function (req,res) {
    referee_dal.update(req.query, function(err, refFName, refLName){
        if (err) {
            res.send(err);
        }
        else {
            referee_dal.getAll(function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('referee/refereeViewAll', {
                        'result': result,
                        'refF': refFName,
                        'refL': refLName,
                        'edit_successful': true
                    });
                }
            });
        }
    });
});


// Delete a player for the given player_id
router.get('/delete', function(req, res){
    if(req.query.ref_id == null) {
        res.send('ref id is null');
    }
    else {
        referee_dal.delete(req.query.ref_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                // change to say success?
                res.redirect(302, '/referee/all');
            }
        });
    }
});

router.get('/eachgames', function (req, res) {
    referee_dal.getGames(function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('referee/viewGames', { 'refs':result });
        }
    });
});

router.get('/viewGamesForRef', function (req, res) {
    if (req.query.ref_id == null) {
        res.send('ref_id is null');
    }
    else {
        referee_dal.getGamesForRef(req.query.ref_id, function (err, games) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('referee/gamesByRefId', {'games': games})
            }
        });
    }
});

module.exports = router;
